package com.countries.test;

import android.app.Activity;
import android.support.test.espresso.Espresso;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.runner.lifecycle.ActivityLifecycleMonitorRegistry;
import android.support.test.runner.lifecycle.Stage;

import com.countries.test.activities.BordersActivity;
import com.countries.test.activities.MainActivity;
import com.countries.test.utils.NetworkUtil;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Collection;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static android.support.test.espresso.matcher.RootMatchers.withDecorView;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isRoot;
import static android.support.test.espresso.matcher.ViewMatchers.withEffectiveVisibility;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsNot.not;

@RunWith(AndroidJUnit4.class)
public class MainActivityInstrumentationTest {
    private Activity mCurrentActivity;

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void checkRecyclerViewAndNavigation() {
        if (NetworkUtil.isNetworkAvailable(mActivityRule.getActivity())) {
            // Check that progress view is visible
            onView(withId(R.id.loading_view)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)));
            // Wait for Retrofit result
            Espresso.registerIdlingResources(mActivityRule.getActivity().getIdlingResource());
            // Check that progress view is gone
            onView(withId(R.id.loading_view)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE)));
            // Apply query to SearchView
            onView(withId(R.id.m_search)).perform(click());
            onView(withId(android.support.design.R.id.search_src_text)).perform(typeText("Israel"));
            // Click on the result
            onView(withId(R.id.countries_recyclerView)).perform(actionOnItemAtPosition(0, click()));
            Espresso.unregisterIdlingResources(mActivityRule.getActivity().getIdlingResource());

            // Wait for Retrofit result
            Espresso.registerIdlingResources(((BordersActivity) getActivityInstance()).getIdlingResource());
            // Check if correct results are displayed
            onView(withText("Egypt")).check(matches(isDisplayed()));
            Espresso.unregisterIdlingResources(((BordersActivity) getActivityInstance()).getIdlingResource());

            // Check navigation
            onView(isRoot()).perform(ViewActions.pressBack());
            onView(isRoot()).perform(ViewActions.pressBack());
            onView(isRoot()).perform(ViewActions.pressBack());
            onView(isRoot()).perform(ViewActions.pressBack());
            // Check if confirm exit toast is displayed
            onView(withText(R.string.confirm_exit))
                    .inRoot(withDecorView(not(is(mActivityRule.getActivity().getWindow()
                            .getDecorView())))).check(matches(isDisplayed()));
        } else {
            // Check that no network dialog is displayed
            onView(withText(R.string.no_connection_dialog_title)).check(matches(isDisplayed()));
            onView(withId(android.R.id.button1)).perform(click());
        }
    }

    private Activity getActivityInstance() {
        getInstrumentation().runOnMainSync(new Runnable() {
            public void run() {
                Collection resumedActivities = ActivityLifecycleMonitorRegistry
                        .getInstance().getActivitiesInStage(Stage.RESUMED);
                if (resumedActivities.iterator().hasNext()) {
                    mCurrentActivity = (Activity) resumedActivities.iterator().next();
                }
            }
        });
        return mCurrentActivity;
    }
}
