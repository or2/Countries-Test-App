package com.countries.test.intrafaces;

import com.countries.test.objects.Country;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface ICountriesService {

    @GET("all")
    Call<ArrayList<Country>> getAllCountries();

    @GET()
    Call<ArrayList<Country>> getCountryBorders(@Url String url);
}