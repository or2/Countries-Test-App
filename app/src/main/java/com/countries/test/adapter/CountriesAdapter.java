package com.countries.test.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.TextView;
import android.widget.Toast;

import com.countries.test.CommonConstants;
import com.countries.test.activities.BordersActivity;
import com.countries.test.activities.MainActivity;
import com.countries.test.R;
import com.countries.test.objects.Country;

import java.util.ArrayList;
import java.util.List;

public class CountriesAdapter extends RecyclerView.Adapter<CountriesAdapter.CountriesVH> {
    private Context mContext;
    private ArrayList<Country> mCountries;
    private ArrayList<Country> mFilteredCountries;

    public CountriesAdapter(Context context, ArrayList<Country> countries) {
        this.mContext = context;
        this.mCountries = countries;
    }

    @Override
    public CountriesVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.country_item, parent, false);
        return new CountriesVH(view);
    }

    @Override
    public void onBindViewHolder(final CountriesVH holder, int position) {
        final ArrayList<String> borders = mCountries.get(holder.getAdapterPosition()).getBorders();
        holder.countryName.setText(mCountries.get(position).getName());
        holder.countryNativeName.setText(mCountries.get(position).getNativeName());
        if (mContext instanceof MainActivity) {
            holder.countryCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (borders.size() > 0) {
                        Intent intent = new Intent(mContext, BordersActivity.class);
                        intent.putExtra(CommonConstants.EXTRA_COUNTRY_NAME, mCountries.get(
                                holder.getAdapterPosition()).getName());
                        intent.putExtra(CommonConstants.EXTRA_COUNTRY_BORDERS, TextUtils.join(";", borders));
                        mContext.startActivity(intent);
                    } else {
                        Toast.makeText(mContext, mContext.getString(R.string.no_borders,
                                mCountries.get(holder.getAdapterPosition()).getName()),
                                Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mCountries == null ? 0 : mCountries.size();
    }

    public void clear() {
        mCountries.clear();
        notifyDataSetChanged();
    }

    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                final FilterResults oReturn = new FilterResults();
                final List<Country> results = new ArrayList<>();
                if (mFilteredCountries == null) {
                    mFilteredCountries = mCountries;
                }
                if (charSequence != null) {
                    if (mFilteredCountries != null && mFilteredCountries.size() > 0) {
                        int size = mFilteredCountries.size();
                        for (int i = 0; i < size; i++) {
                            Country country = mFilteredCountries.get(i);
                            if (country.getName().toLowerCase().contains(charSequence.toString())
                                    || country.getNativeName().toLowerCase().contains(charSequence.toString())) {
                                results.add(country);
                            }
                        }
                    }
                    oReturn.values = results;
                    oReturn.count = results.size();
                }
                return oReturn;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                if (mContext instanceof MainActivity) {
                    TextView textView = (TextView) ((MainActivity) mContext)
                            .findViewById(R.id.no_search_results);
                    if (textView != null) {
                        textView.setVisibility(filterResults.count <= 0 ? View.VISIBLE : View.GONE);
                        textView.setText(mContext.getString(R.string.no_results_for_query, charSequence));
                    }
                }
                mCountries = (ArrayList<Country>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    static class CountriesVH extends RecyclerView.ViewHolder {
        private CardView countryCard;
        private TextView countryNativeName, countryName;

        CountriesVH(View itemView) {
            super(itemView);
            countryCard = (CardView) itemView.findViewById(R.id.country_card);
            countryName = (TextView) itemView.findViewById(R.id.country_name);
            countryNativeName = (TextView) itemView.findViewById(R.id.country_native_name);
        }
    }
}