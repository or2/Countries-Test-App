package com.countries.test.utils;

import com.countries.test.intrafaces.ICountriesService;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CountriesServiceProvider {
    public static final String API_BASE_URL = "https://restcountries.eu/rest/v2/";

    private static Retrofit getRetrofitInstance() {
        return new Retrofit.Builder()
                .baseUrl(API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static ICountriesService getInstance() {
        return getRetrofitInstance().create(ICountriesService.class);
    }
}