package com.countries.test.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Country {
    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("nativeName")
    @Expose
    private String nativeName;

    @SerializedName("borders")
    @Expose
    private ArrayList<String> borders;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNativeName() {
        return nativeName;
    }

    public void setNativeName(String nativeName) {
        this.nativeName = nativeName;
    }

    public ArrayList<String> getBorders() {
        return borders;
    }

    public void setBorders(ArrayList<String> borders) {
        this.borders = borders;
    }
}