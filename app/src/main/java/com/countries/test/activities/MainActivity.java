package com.countries.test.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.test.espresso.idling.CountingIdlingResource;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.countries.test.CommonConstants;
import com.countries.test.R;
import com.countries.test.adapter.CountriesAdapter;
import com.countries.test.intrafaces.ICountriesService;
import com.countries.test.objects.Country;
import com.countries.test.utils.CountriesServiceProvider;
import com.countries.test.utils.NetworkUtil;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    private CountriesAdapter mAdapter;
    private View mLoadingView;
    private SearchView mSearchView;
    private MenuItem mSearchMenuItem;
    private String mQuery;
    public CountingIdlingResource idlingResource = new CountingIdlingResource(
            CommonConstants.COUNTRIES_IDLING_RESOURCE);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme_NoActionBar);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mRecyclerView = (RecyclerView) findViewById(R.id.countries_recyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mLoadingView = findViewById(R.id.loading_view);

        getCountries();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save SearchView query
        if (mSearchView != null) {
            outState.putString(CommonConstants.KEY_SEARCH_QUERY, mSearchView.getQuery().toString());
        }
    }

    @Override
    public void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        // Restore SearchView query
        mQuery = savedInstanceState.getString(CommonConstants.KEY_SEARCH_QUERY);
    }

    long lastPress;

    @Override
    public void onBackPressed() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - lastPress > 3000) {
            Toast.makeText(this, R.string.confirm_exit, Toast.LENGTH_SHORT).show();
            lastPress = currentTime;
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        mSearchMenuItem = menu.findItem(R.id.m_search);

        mSearchView = (SearchView) mSearchMenuItem.getActionView();
        mSearchView.setIconifiedByDefault(true);
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                filter(query);
                mSearchView.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                filter(newText);
                return true;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.m_search:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void filter(String query) {
        if (mAdapter != null) {
            if (query.isEmpty()) {
                mAdapter.getFilter().filter("");
            } else {
                mAdapter.getFilter().filter(query.toLowerCase().trim());
            }
        }
    }

    private void search(String query) {
        if (mSearchView != null && mSearchMenuItem != null) {
            MenuItemCompat.expandActionView(mSearchMenuItem);
            mSearchView.setQuery(query, false);
        }
    }

    private void getCountries() {
        if (NetworkUtil.isNetworkAvailable(this)) {
            idlingResource.increment();

            ICountriesService countriesService = CountriesServiceProvider.getInstance();

            Call<ArrayList<Country>> call = countriesService.getAllCountries();

            call.enqueue(new Callback<ArrayList<Country>>() {
                @Override
                public void onResponse(@NonNull Call<ArrayList<Country>> call, @NonNull Response<ArrayList<Country>> response) {
                    mAdapter = new CountriesAdapter(MainActivity.this, response.body());
                    mRecyclerView.setAdapter(mAdapter);
                    // Apply query if exists
                    if (mQuery != null && !mQuery.isEmpty()) {
                        search(mQuery);
                    }
                    if (mLoadingView != null) {
                        mLoadingView.setVisibility(View.GONE);
                    }
                    idlingResource.decrement();
                }

                @Override
                public void onFailure(@NonNull Call<ArrayList<Country>> call, @NonNull Throwable t) {
                    showErrorDialog(getString(R.string.fetch_error_dialog_title),
                            getString(R.string.fetch_error_dialog_message));
                    if (mLoadingView != null) {
                        mLoadingView.setVisibility(View.GONE);
                    }
                    idlingResource.decrement();
                }
            });
        } else {
            showErrorDialog(getString(R.string.no_connection_dialog_title),
                    getString(R.string.no_connection_dialog_message));
        }
    }

    public CountingIdlingResource getIdlingResource() {
        return idlingResource;
    }

    private void showErrorDialog(String title, String message) {
        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getCountries();
                    }
                })
                .setNegativeButton(R.string.exit, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finishAffinity();
                    }
                })
                .show();
    }
}
