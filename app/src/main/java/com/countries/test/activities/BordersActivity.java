package com.countries.test.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.test.espresso.idling.CountingIdlingResource;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.countries.test.CommonConstants;
import com.countries.test.R;
import com.countries.test.adapter.CountriesAdapter;
import com.countries.test.intrafaces.ICountriesService;
import com.countries.test.objects.Country;
import com.countries.test.utils.CountriesServiceProvider;
import com.countries.test.utils.NetworkUtil;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BordersActivity extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    private CountriesAdapter mAdapter;
    private View mLoadingView;
    private String mCountryName = "", mCountryBorders = "";
    public CountingIdlingResource idlingResource = new CountingIdlingResource(
            CommonConstants.BORDERS_IDLING_RESOURCE);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mCountryName = extras.getString(CommonConstants.EXTRA_COUNTRY_NAME);
            mCountryBorders = extras.getString(CommonConstants.EXTRA_COUNTRY_BORDERS);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (!mCountryName.isEmpty()) {
            toolbar.setTitle(getString(R.string.country_borders_title, mCountryName));
        }
        setSupportActionBar(toolbar);

        mRecyclerView = (RecyclerView) findViewById(R.id.countries_recyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mLoadingView = findViewById(R.id.loading_view);

        getBorders();
    }

    private void getBorders() {
        if (NetworkUtil.isNetworkAvailable(this)) {
            idlingResource.increment();

            ICountriesService countriesService = CountriesServiceProvider.getInstance();

            Call<ArrayList<Country>> call = countriesService.getCountryBorders("alpha?codes=" + mCountryBorders);

            call.enqueue(new Callback<ArrayList<Country>>() {
                @Override
                public void onResponse(@NonNull Call<ArrayList<Country>> call, @NonNull Response<ArrayList<Country>> response) {
                    mAdapter = new CountriesAdapter(BordersActivity.this, response.body());
                    mRecyclerView.setAdapter(mAdapter);
                    if (mLoadingView != null) {
                        mLoadingView.setVisibility(View.GONE);
                    }
                    idlingResource.decrement();
                }

                @Override
                public void onFailure(@NonNull Call<ArrayList<Country>> call, @NonNull Throwable t) {
                    showErrorDialog(getString(R.string.fetch_error_dialog_title),
                            getString(R.string.fetch_error_dialog_message));
                    if (mLoadingView != null) {
                        mLoadingView.setVisibility(View.GONE);
                    }
                    idlingResource.decrement();
                }
            });
        } else {
            showErrorDialog(getString(R.string.no_connection_dialog_title),
                    getString(R.string.no_connection_dialog_message));
        }
    }

    public CountingIdlingResource getIdlingResource() {
        return idlingResource;
    }

    private void showErrorDialog(String title, String message) {
        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getBorders();
                    }
                })
                .setNegativeButton(R.string.exit, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finishAffinity();
                    }
                })
                .show();
    }
}
