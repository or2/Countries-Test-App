package com.countries.test;

public class CommonConstants {
    public static final String KEY_SEARCH_QUERY = "KEY_SEARCH_QUERY";
    public static final String EXTRA_COUNTRY_NAME = "COUNTRY_NAME_EXTRA";
    public static final String EXTRA_COUNTRY_BORDERS = "COUNTRY_BORDERS_EXTRA";
    public static final String COUNTRIES_IDLING_RESOURCE = "COUNTRIES_DATA_LOADER";
    public static final String BORDERS_IDLING_RESOURCE = "BORDERS_DATA_LOADER";
}